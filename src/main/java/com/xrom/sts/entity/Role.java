package com.xrom.sts.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author: XRom
 * @Date: 2018-01-27 13:56:49
 * 实体类：角色
 */
@Data
class Role implements Serializable {

    private Long id;

    private String name;

    private String description;
}
