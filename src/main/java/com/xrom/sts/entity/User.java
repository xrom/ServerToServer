package com.xrom.sts.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @Author: XRom
 * @Date: 2018-01-27 13:33:14
 * 实体类：用户
 */
@Data
public class User implements Serializable {

    private Long id;

    private String username;

    private Date createDate = new Date();

    private Department department;

    private List<Role> roleList;
}
