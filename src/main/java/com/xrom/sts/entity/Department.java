package com.xrom.sts.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author: XRom
 * @Date: 2018-01-27 13:29:48
 * 实体类：部门
 */
@Data
class Department implements Serializable {

    private Long id;    //部门主键id

    private String name;    //部门名称
}
