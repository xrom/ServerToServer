package com.xrom.sts.response;

import lombok.Data;

import java.io.Serializable;


/**
 * @author XRom
 * @date 2018-02-09 10-23
 */
@Data
public class ApiResponse implements Serializable{

    private int code;

    private Object data;

    private String msg;
}

