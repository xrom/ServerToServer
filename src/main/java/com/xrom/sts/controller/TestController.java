package com.xrom.sts.controller;

import com.xrom.sts.entity.User;
import com.xrom.sts.service.ext.RestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @author XRom
 * @date 2018-03-28 10-42
 */
@RestController
@RequestMapping("/test")
public class TestController {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private HttpHeaders httpHeaders;

    @Autowired
    private RestService restService;

    private final String XRomServer = "http://zxr1998.com.cn:812";

    private final String localhost = "http://localhost:812";

    @GetMapping("/hello")
    public String hello() {
        return "hello world!";
    }

    @GetMapping("/rest")
    public User rest() {

        // 请求参数设置
        MultiValueMap<String, String> requestParams= new LinkedMultiValueMap<String, String>();
        requestParams.add("username", "XRom");

        // 方法一
        ResponseEntity<User> response = restTemplate.postForEntity(XRomServer + "/test/findByUsername", requestParams, User.class);
        System.out.println(response);

        //方法二
        ResponseEntity<User> responseEntity = restService.postRequest(XRomServer + "/test/findByUsername", requestParams, User.class);
        System.out.println(responseEntity.getBody());
        return responseEntity.getBody();
    }
}
