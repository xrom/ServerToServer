package com.xrom.sts.service.ext.impl;

import com.xrom.sts.service.ext.RestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

/**
 * @author XRom
 * @date 2018-03-28 17-26
 * restTemplate服务类
 */
@Service
public class RestServiceImpl implements RestService {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private HttpHeaders httpHeaders;

    /**
     * post请求：exchange方式
     * @param url   请求地址
     * @param params    请求参数集合
     * @param responseType  返回的数据类型
     * @param <T>   泛型：返回的数据类型
     * @return
     */
    @Override
    public <T> ResponseEntity<T> postRequest(String url, MultiValueMap<String, String> params, Class<T> responseType) {
        httpHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(params, httpHeaders);
        return restTemplate.exchange(url, HttpMethod.POST, requestEntity, responseType);
    }

}
