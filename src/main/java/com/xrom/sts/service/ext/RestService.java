package com.xrom.sts.service.ext;

import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;

public interface RestService {

    <T> ResponseEntity<T> postRequest(String url, MultiValueMap<String, String> params, Class<T> responseType);
}
